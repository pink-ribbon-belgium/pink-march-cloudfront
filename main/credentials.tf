provider "aws" {
  region  = local.region
  profile = local.profile
  version = "~> 2.62"
  assume_role {
    role_arn = data.terraform_remote_state.infrastructure_role.outputs.I-infrastructure_role["arn"]
  }
}

