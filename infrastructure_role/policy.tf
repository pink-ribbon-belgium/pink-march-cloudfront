data "aws_iam_policy_document" "role-policy" {
  statement {
    effect = "Allow"
    actions = [
      "cloudfront:CreateDistribution",
      "cloudfront:CreateDistributionWithTags",
      "cloudfront:ListDistributions",
      "cloudfront:DeleteDistribution",
      "cloudfront:GetDistribution",
      "cloudfront:GetDistributionConfig",
      "cloudfront:UpdateDistribution",
      "cloudfront:TagResource",
      "cloudfront:UntagResource",
      "cloudfront:ListTagsForResource"
    ]
    resources = ["*"]
  }
  statement {
    effect = "Allow"
    actions = [
      "route53:ListHostedZones",
      "route53:GetHostedZone",
      "route53:GetChange",
      "route53:ListTagsForResource",
      "route53:ChangeTagsForResource",
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets"
    ]
    resources = ["*"]
  }
  statement {
    effect = "Allow"
    actions = [
      "acm:AddTagsToCertificate",
      "acm:DescribeCertificate",
      "acm:DeleteCertificate",
      "acm:ExportCertificate",
      "acm:GetCertificate",
      "acm:ListCertificates",
      "acm:ListTagsForCertificate",
      "acm:RemoveTagsFromCertificate",
      "acm:RenewCertificate",
      "acm:RequestCertificate",
      "acm:UpdateCertificateOptions",
    ]
    resources = ["*"]
  }
  statement {
    effect    = "Allow"
    actions   = ["lambda:GetFunction"]
    resources = ["${local.lambda-edge-id}:*"]
  }
  statement {
    effect    = "Allow"
    actions   = ["lambda:EnableReplication"]
    resources = [local.lambda-edge-id]
  }
  statement {
    effect = "Allow"
    actions = [
      "iam:CreateServiceLinkedRole"
    ]
    resources = ["*"]
  }
}

# Attach the role to the policy
resource "aws_iam_policy" "role-policy" {
  name        = local.infrastructure-prefix
  policy      = data.aws_iam_policy_document.role-policy.json
  path        = "/devsecops/"
  description = ""
}
