output "I-infrastructure_role" {
  value = {
    path = aws_iam_role.pink-march-cloudfront-infrastructure.path
    name = aws_iam_role.pink-march-cloudfront-infrastructure.name
    arn  = aws_iam_role.pink-march-cloudfront-infrastructure.arn
  }
}
