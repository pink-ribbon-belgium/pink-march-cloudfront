# pink-march-cloudfront Terraform Infrastructure Definition Role

The role that has the minimal privileges to manage the infrastructure necessary for the Pink March CloudFront.

## Getting started

The infrastructure is defined using [Terraform].
See [Getting started with a Terraform configuration].

You need to have [AWS credentials] set up in `~/.aws/credentials` for profile `pink-ribbon-belgium-dev`,
which should have `devsecops` privileges. to be able to define this role.

## Structure

This role is assumed when applying the infrastructure defined in [`main/`](main/README.md). It applies the infrastructure.

[terraform]: https://peopleware.atlassian.net/wiki/x/CwAvBg
[getting started with a terraform configuration]: https://peopleware.atlassian.net/wiki/x/p4zhC
