locals {
  region                = "eu-west-1"
  profile               = "pink-ribbon-belgium-dev"
  repo-basename         = "pink-march-cloudfront"
  infrastructure-prefix = "${local.repo-basename}-infrastructure"
  lambda-edge-id        = "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:pink-march-header-adder"
}

data "aws_caller_identity" "current" {}
