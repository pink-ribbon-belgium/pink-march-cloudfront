locals {
  bookmarkable_version   = "00142"
  origin-ui_bucket       = data.terraform_remote_state.ui_s3_bucket.outputs.I-ui_bucket["id"]
  root_max_cache_seconds = 300
  bookmarkable_origin    = "bookmarkable"
  patience_origin        = "patience"
  api_origin             = "api"
  api_origin-domain_name = "c0ld802vcd.execute-api.eu-west-1.amazonaws.com" # MUDO get from service configuration
}

resource "aws_cloudfront_distribution" "pink-march" {
  comment             = "Gateway for the Pink March 2019 App"
  wait_for_deployment = false
  enabled             = true
  is_ipv6_enabled     = true
  price_class         = "PriceClass_100"

  aliases = concat(list(local.cf_domain_name-main), local.cf_domain_name-alternates)

  default_root_object = "index.html"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  # MUDO add logging

  viewer_certificate {
    acm_certificate_arn      = module.cert.I-arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2018"
  }

  tags = local.tags

  origin {
    origin_id   = local.origin-ui_bucket
    domain_name = data.terraform_remote_state.ui_s3_bucket.outputs.I-ui_bucket["bucket_regional_domain_name"]
    s3_origin_config {
      origin_access_identity = data.terraform_remote_state.ui_s3_bucket.outputs.I-ui_bucket-cloudfront_origin_access_identity["path"]
    }
  }

  origin {
    origin_id   = local.patience_origin
    domain_name = data.terraform_remote_state.ui_s3_bucket.outputs.I-ui_bucket["bucket_regional_domain_name"]
    origin_path = "/ui/patience"

    s3_origin_config {
      origin_access_identity = data.terraform_remote_state.ui_s3_bucket.outputs.I-ui_bucket-cloudfront_origin_access_identity["path"]
    }
  }

  origin {
    origin_id   = local.bookmarkable_origin
    domain_name = data.terraform_remote_state.ui_s3_bucket.outputs.I-ui_bucket["bucket_regional_domain_name"]
    origin_path = "/ui/bookmarkable/${local.bookmarkable_version}"

    s3_origin_config {
      origin_access_identity = data.terraform_remote_state.ui_s3_bucket.outputs.I-ui_bucket-cloudfront_origin_access_identity["path"]
    }
  }

  origin {
    origin_id   = local.api_origin
    domain_name = local.api_origin-domain_name
    origin_path = ""

    custom_origin_config {
      // http port is irrelevant, but required
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "https-only"

      origin_ssl_protocols = [
        "TLSv1.1",
        "TLSv1.2",
      ]

      origin_read_timeout      = 45
      origin_keepalive_timeout = 60
    }
  }

  ordered_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.bookmarkable_origin

    path_pattern = "/ngsw.json"

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    max_ttl     = local.root_max_cache_seconds
    default_ttl = local.root_max_cache_seconds

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  ordered_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.bookmarkable_origin

    path_pattern = "/fitbit.html"

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    max_ttl     = local.root_max_cache_seconds
    default_ttl = local.root_max_cache_seconds

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  ordered_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.bookmarkable_origin

    path_pattern = "/googlefit.html"

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    max_ttl     = local.root_max_cache_seconds
    default_ttl = local.root_max_cache_seconds

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  ordered_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.bookmarkable_origin

    path_pattern = "/polar.html"

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    max_ttl     = local.root_max_cache_seconds
    default_ttl = local.root_max_cache_seconds

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  # MAIN PRODUCTION ENTRY POINT
  ordered_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods  = ["GET", "HEAD"]
    # NOTE: temporarily set to patience
    #target_origin_id = local.bookmarkable_origin
    target_origin_id = local.patience_origin

    path_pattern = "/index.html"

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    max_ttl     = local.root_max_cache_seconds
    default_ttl = local.root_max_cache_seconds

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type   = "origin-response"
      lambda_arn   = local.header_appender_lambda
      include_body = false
    }
  }

  ordered_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.bookmarkable_origin

    path_pattern = "/ngsw-worker.js"

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    max_ttl     = local.root_max_cache_seconds
    default_ttl = local.root_max_cache_seconds

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type   = "origin-response"
      lambda_arn   = local.header_appender_lambda
      include_body = false
    }
  }

  ordered_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.origin-ui_bucket

    path_pattern = "/*index.html"

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    max_ttl     = local.root_max_cache_seconds
    default_ttl = local.root_max_cache_seconds

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type   = "origin-response"
      lambda_arn   = local.header_appender_lambda
      include_body = false
    }
  }

  ordered_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.origin-ui_bucket

    path_pattern = "/ui/bookmarkable/*ngsw-worker.js"

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    max_ttl     = local.root_max_cache_seconds
    default_ttl = local.root_max_cache_seconds

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type   = "origin-response"
      lambda_arn   = local.header_appender_lambda
      include_body = false
    }
  }

  ordered_cache_behavior {
    path_pattern           = "api-*"
    target_origin_id       = local.api_origin
    viewer_protocol_policy = "https-only"

    allowed_methods = [
      "HEAD",
      "DELETE",
      "POST",
      "GET",
      "OPTIONS",
      "PUT",
      "PATCH",
    ]

    // no caching of OPTIONS
    # "CloudFront caches responses to GET and HEAD requests and, optionally, OPTIONS requests. CloudFront does not
    #  cache responses to requests that use the other methods."
    #
    # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/distribution-web-values-specify.html#DownloadDistValuesAllowedHTTPMethods
    # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/distribution-web-values-specify.html#DownloadDistValuesCachedHTTPMethods
    cached_methods = [
      "HEAD",
      "GET",
    ]

    compress = true

    // 5 seconds to tackle DoS
    default_ttl = 5
    min_ttl     = 5
    max_ttl     = 5

    forwarded_values {
      // nothing to forward for the api
      cookies {
        forward = "none"
      }

      query_string = true

      # Intended as DDOS prevention on GET
      # For authorized resources, this only caches for one token (1 user).
      # For open resources (health), this caches for 5 seconds, but takes x-force-error and x-log-level into
      # account.
      # We do not want to mention x-flow-id here, because each call has a different value, and there would effectively
      # be no caching.
      #
      # "Forward a whitelist of headers that you specify. CloudFront caches your objects based on the values in all of
      #  the specified headers. CloudFront also forwards the headers that it forwards by default, but it caches your
      #  objects based only on the headers that you specify."
      #
      # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/header-caching.html#header-caching-web
      #
      # Cloudfront forwards all headers not in the list (see 'Other-defined headers'), but does not cache on them.
      # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/RequestAndResponseBehaviorCustomOrigin.html#request-custom-headers-behavior
      headers = [
        "Accept",
        "Accept-Language",
        "Authorization",
        "Referer",
        "CloudFront-Viewer-Country"
      ]

      // don't cache on any query strings
      query_string_cache_keys = [
        "doesnotexist",
      ]
    }

    smooth_streaming = false
  }

  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = local.origin-ui_bucket
    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

}
