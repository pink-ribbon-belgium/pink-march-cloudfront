locals {
  region  = "eu-west-1"
  profile = "pink-ribbon-belgium-dev"
  repo    = "pink-march-cloudfront"
  tags = {
    name = "Pink March Cloudfront configuration"
    env  = "production"
    repo = "${local.repo}//terraform/main"
  }
  # Depending on versions, an FQDN received from AWS might or might not end with a trailing dot `"."`.
  # We remove the dot, if it occurs.
  root_domain-clean     = replace(data.terraform_remote_state.dns.outputs.I-hosted_zone-pink_ribbon_belgium_org["name"], "/[.]$/", "")
  cf_domain_name-domain = "pink-march.${local.root_domain-clean}"
  cf_domain_name-main   = "app.${local.cf_domain_name-domain}"
  cf_domain_name-alternates = [
    "demo.${local.cf_domain_name-domain}",
    "june2020.${local.cf_domain_name-domain}",
    "october2020.${local.cf_domain_name-domain}"
  ]
  header_appender_lambda = "arn:aws:lambda:us-east-1:${data.aws_caller_identity.current.account_id}:function:pink-march-header-adder:15"
}

data "aws_caller_identity" "current" {}
