resource "aws_route53_record" "app-ipv4" {
  name    = local.cf_domain_name-main
  type    = "A"
  zone_id = data.terraform_remote_state.dns.outputs.I-hosted_zone-pink_ribbon_belgium_org["id"]

  alias {
    evaluate_target_health = true
    name                   = aws_cloudfront_distribution.pink-march.domain_name
    zone_id                = aws_cloudfront_distribution.pink-march.hosted_zone_id
  }
}

resource "aws_route53_record" "app-ipv6" {
  name    = local.cf_domain_name-main
  type    = "AAAA"
  zone_id = data.terraform_remote_state.dns.outputs.I-hosted_zone-pink_ribbon_belgium_org["id"]

  alias {
    evaluate_target_health = true
    name                   = aws_cloudfront_distribution.pink-march.domain_name
    zone_id                = aws_cloudfront_distribution.pink-march.hosted_zone_id
  }
}

resource "aws_route53_record" "alternate-ipv4" {
  count = length(local.cf_domain_name-alternates)

  name    = local.cf_domain_name-alternates[count.index]
  type    = "A"
  zone_id = data.terraform_remote_state.dns.outputs.I-hosted_zone-pink_ribbon_belgium_org["id"]

  alias {
    evaluate_target_health = true
    name                   = aws_cloudfront_distribution.pink-march.domain_name
    zone_id                = aws_cloudfront_distribution.pink-march.hosted_zone_id
  }
}

resource "aws_route53_record" "alternate-ipv6" {
  count = length(local.cf_domain_name-alternates)

  name    = local.cf_domain_name-alternates[count.index]
  type    = "AAAA"
  zone_id = data.terraform_remote_state.dns.outputs.I-hosted_zone-pink_ribbon_belgium_org["id"]

  alias {
    evaluate_target_health = true
    name                   = aws_cloudfront_distribution.pink-march.domain_name
    zone_id                = aws_cloudfront_distribution.pink-march.hosted_zone_id
  }
}

