terraform {
  backend "s3" {
    bucket         = "tfstate.pink-ribbon-belgium.org"
    key            = "pink-march-cloudfront.tfstate"
    region         = "eu-west-1"
    profile        = "pink-ribbon-belgium-dev"
    encrypt        = true
    dynamodb_table = "tfstate-lock.pink-ribbon-belgium.org"
  }
}
