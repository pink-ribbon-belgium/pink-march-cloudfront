module "cert" {
  source                  = "github.com/peopleware/terraform-ppwcode-modules//tlsCertificate?ref=v%2F7%2F0%2F0"
  terraform-configuration = "${local.repo}//terraform/main"
  environment             = "production"
  zone_id                 = data.terraform_remote_state.dns.outputs.I-hosted_zone-pink_ribbon_belgium_org["id"]
  region                  = "us-east-1"
  profile                 = "pink-ribbon-belgium-dev"
  main_fqdn               = local.cf_domain_name-main
  alternate_fqdns         = local.cf_domain_name-alternates
}
