# pink-march-cloudfront Terraform Infrastructure Definition - main

The infrastructure necessary for the Pink March CloudFront.

## Getting started

The infrastructure is defined using [Terraform].
See [Getting started with a Terraform configuration].

You need to have [AWS credentials] set up in `~/.aws/credentials` for profile `pink-ribbon-belgium-dev`,
which should be able to assume the role defined in [`infrastructure_role/`](infrastructure_role).

## Adding alternate names

This Cloudfront distribution features several alternate domain names (see [`constants.tf`](constants.tf).
All the domain names used have to be included in the TLS certificate, and referred to in the Cloudfront distribution.

The latter is not an issue.

For maintenance of the TLS certificate, we use `terraform-ppwcode-modules//tlsCertificate`, which

- create a certificate request
- creates DNS entries to validate the request
- waits for validation of the request

The creation of the DNS entries to validate the request has major issues
(see https://github.com/terraform-providers/terraform-provider-aws/issues/6557).
A workaround is described, but then still the process often fails in the middle when alternate names are added.
The state is then unstable, and it becomes necessary to delete "half completed" stuff as admin in the console.

When the described workaround in [`tlsCertificate/proof.tf`](.terraform/modules/cert/tlsCertificate/proof.tf) is
applied, and resource records are deleted by hand when [Terraform] asks for it, we eventually get there.
This is far from the described "zero downtime":" in the `terraform-ppwcode-modules//tlsCertificate` documentation,
that was intended.

Note that not using the module, and doing what is needed here directly by hand would not resolve the issue.
We would do exactly what the module does.

We did not find a better solution yet.

In conclusion, to add domain names:

- add them to [`constants.tf`](constants.tf) `cf_domain_name-alternates`
- try to `terraform apply`; if it fails, comment out
    - [`tlsCertificate/proof.tf`](.terraform/modules/cert/tlsCertificate/proof.tf),
    - [`tlsCertificate/main.tf`](.terraform/modules/cert/tlsCertificate/main.tf) `aws_acm_certificate_validation.tls_certificate`,
  and apply
- uncomment what was commented, and apply again
- delete stuff by hand when [Terraform] says it already exists, and re-apply, until stable

[terraform]: https://peopleware.atlassian.net/wiki/x/CwAvBg
[getting started with a terraform configuration]: https://peopleware.atlassian.net/wiki/x/p4zhC
[aws credentials]: https://peopleware.atlassian.net/wiki/x/RoAWBg
